**An Architectural Style for IoT Systems**


An IoT architectural style can be used as a starting point for a coherent modeling of IoT systems, guiding the architect on the way IoT components should be combined.

This IoT style is compatible with the IoT conceptual model defined by the ISO/IEC 30141 Standard, which provides the common structure and definitions for describing the entities of IoT systems and the relationships among them.

The IoT style was specified through a set of constraints that characterize the style, in addition to the structural and behavioral views of the architectural elements of the style, defined in the  [SysADL language](http://sysadl.org).

The SysADL Studio tool allows the validation of the implementation of the IoT style in SysADL architectural specifications.
To demonstrate the use of the IoT style, three architectural IoT projects were developed: (i) Smart Place, (ii) Angel Wristband and (iii) Smart Home.
To evaluate the IoT style, a controlled experiment was performed.